This program will allow the user to take IV curves and analyze the data.

To take an IV curve, the power supply voltage is run in constant current with a voltage limit and steps through different current values.

The slope and the offset of the IV curve is then fit to the data.

### Contents
1. [Getting started](#getting-started)
  * [First time setup](#first-time-setup)
  * [After first setup](#after-first-setup)
2. [Description of the code](#description-of-the-code)
  * [Input JSON file](#input-json-file)
  * [Python script](#python-script)
3. [Taking IV curves](#taking-iv-curves)
4. [Analyzing IV curves](#analyzing-iv-curves)

----

# Getting started

### First time setup
First clone this project.
<pre>
git clone https://gitlab.cern.ch/YARR/utilities/lowvoltageiv.git
</pre>

Exit the `lowvoltageiv` directory:
<pre>
cd ..
</pre>

Clone the labRemote application called `simple-reader-writer`
<pre>
git clone --recursive https://gitlab.cern.ch/berkeleylab/labremote-apps/simple-reader-writer.git
</pre>

Note: the recursive statement is important to also clone the labRemote submodule.

To compile simple-reader-writer:
<pre>
cd simple-reader-writer
mkdir build
cd build
cmake3 ../
make
</pre>

Exit the `simple-reader-writer` directory
<pre>
cd ../..
</pre>

Clone the Yarr repository
<pre>
git clone https://gitlab.cern.ch/YARR/YARR.git
</pre>

You need to build Yarr. For more information about how to do this, please refer to: [Read the docs](https://yarr.readthedocs.io/en/latest/install/)
This will describe the YARR installation

### After first setup
The IV code taking itself is a python script so there is no need to compile if you make changes to this code. Only changes to the YARR or Simple Reader Writer files require compiling.


# Description of the code

The IV curve code consists of one program `makeIVCurve.py` and a json file for input parameters `ivcurvescan.json`

### Input JSON file
The `ivcurvescan.json` sets all the parameters used to make IV curves. The parameters in `ivcurvescan.json` are split into 3 categories:
- `setup`: describes all the YARR settings. 
    - `controller`: file name for the spec config
    - `connectivity`: connectivity configuration
    - `YarrPath`: path to `YARR` folder. Default: assume Yarr directory is in the same folder as `simple-reader-writer` and `lowvoltageiv`
- `labRemote`: all the power supply settings
    - All commands assume a base command that sets up the path to the `simple-reader-writer` command
        - Assumes `simple-reader-writer` is in the same directory as `lowvoltageiv`
        - The base command is:
        <pre>
        ./../simple-reader-writer/build/bin/simple_agilent_measure -g 6 -p /dev/ttyUSB0 -c -1
        </pre>
        - `-g`: GPIB address of the PS, default is 6
        - `-c`: channel of the PS. If the PS only has one channel, use -1. Default is -1
        - `-p`: USB port. Default is `/dev/ttyUSB0`
    - `readVoltageCmd`: command to read the voltage
    - `readCurrentCmd`: command to read the current
    - `setVoltCurrCmd`: command to turn on the PS and set the voltage and current. In the IV curve script, user sends voltage and current as parameters
    - `powerOffCmd`: command to turn off the PS. 
    - `powercycle`: set to 1 if you would like to powercycle the PS between each IV curve step
- `settings`: settings for the IV curve data taking
    - `NRepititons`: how many IV curves you would like to take
    - `configChip`: set to 1 if you would like to configure the chip at each step of the IV curve
    - `maxVoltage`: voltage limit for the power supply. Currently the max voltage allowed is 2.2V
    - `start`: start current (minimum suggested start current is 0.1 A)
    - `end` : end current for the ramp. Max end current for SCC is 2.0 A, max end current for Triplet is 4.5A
    - `nSteps`: how many current values to go through for your IV curve
    - `sleep`: how long to wait after you set the PS current and voltage before configuring the chip or reading back the PS values
    - `chipType`: what kind of chip (RD53a, RD53b)
    - `chipName`: chip name from production database
    - `boardtype`: SCC or triplet? Used to determine if the user is inputting too high of a current
    - `outFile`: name of output file to store the IV curve data
    - `debug`: set to 1 if you want debug statements printed out

### Python script
The python script used to take IV curves is called `makeIVcurve.py`.

The code checks the max voltage and the current endpoints to ensure that they are not set to too high values.

It then configures the PS voltage and current with the values specified by the user. To ensure that the PS is in `constant current`, the voltage limnit should be set higher than the chip consumed voltage.

The PS current and voltage are then read back and stored for the future analysis.

Only the current value is changed during the data taking. 

If you choose to configure the chip, the return status of the configuration will be saved. The status is `0` if the chip has successfully configured. This will tell you at what minimum current you are able to configure the chip.

After running, there will be output json file(s) which store the the voltage, current, and if the chip configured (called `Status`). 


# Taking IV curves

The script to take IV curves is called `makeIVcurve.py`. The script can be run either using `python` or `python3`.
<pre>
python makeIVcurve.py
</pre>

# Analyzing IV curves

The script to analyze IV curves is called `analyzeIV.py`. The script currently runs in `python` but not `python3`. 

In order to run this script, you need matplotlib and numpy installed
<pre>
sudo yum install python-matplotlib
</pre>

There are a few input options for the script:
- `-i` or `--ifile`: input file path and file name without the .json extension. The script will get all occurrences of the file name to analyze
- `-o` or `--ofile`: output file name
- `-s` or `--start`: start voltage value for the fit
- `-e` or `--end`: end voltage value for the fit

How to run the analysis script:
<pre>
python analyzeIV.py -i example_output/IVout_test
</pre>

With all options,
<pre>
python analyzeIV.py -i example_output/IVout_test -o IVout -s 1.5 -e 1.75
</pre>

An example output looks like

<img src="example_output/0x0796_test.png" alt="IV curve example" width="600">

If the chip configuration was attempted during the fit, a vertical line is drawn on the plot at the current where the chip starts configuring.