# Code used to analyze IV curves
# inputs:
#    ifile: input file name (i.e. IVout_test)
#    ofile: output file name
#    start: voltage value at which to start fit
#    end: voltage value at which to end fit
# outputs: saves a plot of IV curve with fit function as .png, .eps, .pdf
# author: E. Resseguie
from pylab import *
import numpy
import matplotlib.pyplot as plt
import os, glob, getopt
import json

matplotlib.rcParams.update({'font.size': 12}) #sets the font on the plots

colors='blue'

# function to plot the IV curve and fit a linear function
# saves the output as .png, .eps, .pdf
# description of the arguments:
# x: x variable to plot, y: y variable to plot
# idxstart: index from which to start the fit
# idxend: index to which to end the fit
# config: draw vertical line at this index
# xtitle: x-axis title, ytitle: y-axis title
# title: plot title
# save: output 
def superpose(x,y, idxstart, idxend, config, xtitle, ytitle, title, save,yerr=None):
    fig, ax = plt.subplots()

    # Plot the current and voltage
    if (yerr == None):
        ax.plot(x,y, ls='', marker='.')
    else:
        ax.errorbar(x, y, yerr = yerr, linestyle = "None", marker='.',markersize=5, color=colors, label = '')

    # determine the range to start the fit
    curr=x
    volt=y
    coefficients = np.polyfit(curr[idxstart:idxend], volt[idxstart:idxend], 1)
    polynomial = np.poly1d(coefficients)

    # plot the fit and add a label
    x_axis = np.linspace(curr[idxstart],curr[idxend],10)
    y_axis = polynomial(x_axis)
    #ax.plot(x_axis, y_axis, color=colors, label='slope: %.2f $\\Omega$, offset: %0.2f V'%(coefficients[0],coefficients[1]))
    ax.plot(x_axis, y_axis, color=colors, label='slope: %.2f ohm, offset: %0.2f V'%(coefficients[0],coefficients[1]))

    # plot line where chip configures
    if not(config == None):
        plt.axvline(x=curr[config], color='gray', linestyle='--')
        text(curr[config]-0.05, volt[config-1], "Chip configures",rotation=90, verticalalignment='center')
        
    ax.set_title(title,fontsize=12)
    plt.ylim(0.5, 2.0)
    ax.legend(loc="upper left", fontsize=12)
    ax.set_ylabel(ytitle,fontsize=12)
    ax.set_xlabel(xtitle,fontsize=12)
    ax.grid(True)

    # save formats
    plt.savefig(save+'.png')
    plt.savefig(save+'.eps')
    plt.savefig(save+'.pdf')

    plt.clf()

def main(argv):
   inputfile = ''
   outputfile = ''
   minVolt = None
   maxVolt = None
   
   opts, args = getopt.getopt(argv,"hi:o:s:e:",["ifile=","ofile="])
   if len(opts) == 0:
      print('Missing input filename and output filname!')
      print('Try analyzeIV.py -i <inputfile> -o <outputfile>')
      print('Do not put file extensions for the input or output file names')
      print('For full list of options run analyzeIV.py -h')
   for opt, arg in opts:
      if opt == '-h':
         print('analyzeIV.py -i <inputfile> -o <outputfile> -s <start> -e <end>')
         print('Do not put file extensions for the input or output file names')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-s", "--start"):
          minVolt = float(arg)
      elif opt in ("-e", "--end"):
          maxVolt = float(arg)

   if inputfile == '':
      print('Missing input file. Try analyzeIV.py -i <inputfile>')
      sys.exit()

   if outputfile == '': outputfile = "test"
   if minVolt == None: minVolt = 1.6
   
   #getting the list of input files
   list_of_files = glob.glob(inputfile+'*.json')
   numAvg = len(list_of_files)
   
   #define arrays used for the fit
   curr_avg, curr_std, volt_avg, volt_std, status = ([] for i in range(0,5))
   tmp_curr, tmp_volt, tmp_status=([] for i in range (0,3))
   curr, volt=([] for i in range(0,2))

   chipName=''
   
   #start processing the files one by one
   for fileName in list_of_files:

       #open json file to read in the current, voltage, and chip status code
       with open(fileName) as json_file:
           data = json.load(json_file)
           chipName = data['chipName']
           values = data['values']
           tmp_curr.append(values['Current'])
           tmp_volt.append(values['Voltage'])

           if not len(values['Status']) == 0: tmp_status.append(values['Status'])

   #calculate average and standard deviations of current, voltage
   tmp_curr = np.array(tmp_curr)
   tmp_volt = np.array(tmp_volt)

   for i in range(0, len(tmp_curr[0])):
       curr_avg.append(np.mean(tmp_curr[:,i])) 
       curr_std.append(np.std(tmp_curr[:,i]))
       volt_avg.append(np.mean(tmp_volt[:,i]))
       volt_std.append(np.std(tmp_volt[:,i]))
           
   #determine indices for the fit, and for when the chip starts configuring
   idxstart = next(x for x, val in enumerate(volt_avg) if val < minVolt)
   
   if maxVolt == None: idxend = 0
   else: idxend = next(x for x, val in enumerate(volt_avg) if val > maxVolt)

   #determine index at which chip configures
   config = None
   if len(tmp_status) >	0:
       tmp_status = np.array(tmp_status)
       for i in range(0, len(tmp_curr[0])):
           status.append(np.mean(tmp_status[:,i]))
       config = next(x for x, val in enumerate(status) if val > 0)
       
   #make plots
   superpose(curr_avg,volt_avg,idxend, idxstart, config, 'Current [A]','Voltage [V]', chipName+' IV curve averaged over '+str(numAvg)+' measurements', chipName+'_'+outputfile,volt_std)
        
if __name__ == "__main__":
   main(sys.argv[1:])
   




