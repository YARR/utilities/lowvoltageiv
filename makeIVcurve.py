# Code to take IV curves using settings from a json
# Output the current and voltage to a json
# Author: Elodie Resseguie 

import os, subprocess
import json
import time

#open and read in JSON files
filename='ivcurvescan.json'
f = open(filename, 'r')
datastore = json.load(f)
        
#get YARR setup parameters
setup=datastore['setup']
if not len(setup['controller']) == 0: controller = setup['controller']
if not len(setup['connectivity']) == 0: connectivity = setup['connectivity']
if not len(setup['YarrPath']) == 0: yarrpath = setup['YarrPath']

# get IV curve parameters
settings=datastore['settings']
if not(settings['NRepititons'] == None): nrep = settings['NRepititons']
if not(settings['configChip'] == None): configChip = settings['configChip']
if not len(settings['boardtype']) ==0: brd = settings['boardtype']
if not(settings['start'] == None): start = settings['start']
if not settings['end'] == None: 
    end = settings['end']
    if end > 4.5 and brd == 'Triplet':
        print("Max current of 4.5A exceeded. Ramp end set to 4.5")
        end = 4.5
    if end > 2.0 and (brd == 'SCC' or brd == 'hybrid'):
        print("Max current of 1.5A exceeded. Ramp end set to 2.0")
        end = 2.0
if not settings['maxVoltage'] == None:
    maxvoltage = settings['maxVoltage']
    if maxvoltage > 2.2:
        print("Max voltage exceeded. Set to 2.2V")
        end = 2.2
if not(settings['debug'] == None): debug = settings['debug']

if not settings['nSteps'] ==None: step = (end-start)/float(settings['nSteps'])
if not len(settings['boardtype']) ==0: brd = settings['boardtype']
if not len (settings['chipType']) == 0: chipType = settings['chipType']
if not len(settings['chipName']) == 0: chipName = settings['chipName']
if not settings['sleep'] == None: sleep = settings['sleep']
if not len(settings['outFile']) ==0: outFile = settings['outFile']

# get lab remote parameters
labremote=datastore['labRemote']
if not labremote['powercycle'] == None: powercycle = labremote['powercycle']

# configure default Yarr and labRemote commands
defaultYarrCmd = yarrpath + 'bin/scanConsole -r ' + yarrpath + 'configs/controller/' + controller + ' -c ' + yarrpath + 'configs/connectivity/' + connectivity 
readVoltageCmd = labremote['readVoltageCmd']
readCurrentCmd = labremote['readCurrentCmd']
setVoltCurrCmd = labremote['setVoltCurrCmd']
powerOffCmd = labremote['powerOffCmd']

# Start taking IV curve with specs defined above
for i in range (0, nrep):
        
    # create output list to store in json  
    voltage = []
    current = []
    chipStatus = []

    count = end

    while (count >= start):
        # power cycle the PS
        if powercycle: 
            if debug: print('Powering off the PS')
            os.system(powerOffCmd)
            time.sleep( 1 )

        # set the PS voltage and current
        print('Setting the PS voltage %0.2f, current %0.2f'%(maxvoltage, count))
        os.system(setVoltCurrCmd + ' ' +str(maxvoltage) +' '+  str(count))
        time.sleep( sleep ) 

        # if configuring the chip, check the status
        if configChip == 1:
            chipstate = (os.system( defaultYarrCmd))
            if debug: print('Chip status after configuration: ' + str(chipstate))
            chipStatus.append(chipstate)

        # read PS current and voltage
        pvolt = os.popen(readVoltageCmd)
        tmp_volt = pvolt.read().split('\n')[0]
        voltage.append(float(tmp_volt))
        pcurr = os.popen(readCurrentCmd)
        tmp_curr = pcurr.read().split('\n')[0]
        current.append(float(tmp_curr))
        if debug: print ('current: ' + tmp_curr + ', voltage: ' + tmp_volt)

        # increment the current
        count = count - step
        
    # write to JSON file
    with open(outFile+'_'+str(i)+'.json', 'w') as fout:
        json.dump({'chipType': chipType, 'scanType': 'IVcurve', 'chipName': chipName, 'values':{'Voltage': voltage, 'Current': current, 'Status': chipStatus}}, fout, indent=2, sort_keys=True, separators=(',',':'))

# turn off PS at the end of the IV curves
if debug: print("Turning off the PS, IV curves are done")
os.system(powerOffCmd)


